import {Component} from 'angular2/core'
import {CourseService} from './course.service'
import {AutoGrowDirective} from './auto-grow.directive'
@Component({
    selector:'courses',
    template:`
        <p>From Courses Component Help</p>
        <i class="glyphicon" 
        [class.glyphicon-star-empty]="isEmptyStarr"
        [class.glyphicon-star]="!isEmptyStarr" (click)="onClickStar()">
        </i>
        <div (click)="onBtnClcikFromDiv()">
            <button [style.backgroundColor]="isActive ? 'blue' : 'red' " (click)="onBtnClcik($event)">Click</button>
        </div>
        <input type="text" autoGrow />
        <h1>{{title}}</h1>
        <ul>
            <li *ngFor="#course of courses"> {{course}}</li>
        </ul>
        
    `,
    providers:[CourseService],
    directives:[AutoGrowDirective]
})
export class CoursesComponent{
    title = "Title of course";
    isEmptyStarr = true;
    isActive = true;
    courses;
    constructor(courseService: CourseService)
    {
        this.courses = courseService.getCourse();
    }
    onBtnClcik($event){
        $event.stopPropagation();
        console.log("clicked from button");
    }
    onBtnClcikFromDiv()
    {
        console.log("clicked from button div div");
    }
    onClickStar()
    {
        this.isEmptyStarr = !this.isEmptyStarr;
    }
}