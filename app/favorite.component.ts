import {Component, Input, Output, EventEmitter} from 'angular2/core'

@Component({
    selector: 'favo',
    templateUrl:'app/favorite.template.html',
    styles:[`
        .glyphicon-star{
            color:orange
        }
    `]

})
export class FavoriteComponent{
    @Input('is-empty') isEmptyStarr = false;
    @Output() change = new EventEmitter();
    onClickStar($event)
    {
        this.isEmptyStarr = !this.isEmptyStarr;
        this.change.emit({changedto:$event})
    }

}