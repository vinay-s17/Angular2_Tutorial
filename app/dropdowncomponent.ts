import{Component, Input, Output, EventEmitter, ElementRef} from 'angular2/core';
import{DropdownValue} from './dropdownvalue';

@Component({
  selector: 'st-dropdown',
  template: `
  <select [(ngModel)]="selectedItemId" (change)="selectedItem($event)" 
  class="selectpicker" data-style="btn-new">
  <option value=null selected="selected">{{placeholder}}</option>
  <option *ngFor="#obj of values" [value]="obj.value">{{obj.label}}</option>
</select>

  `
})
export class DropdownComponent {

  simpleValue:number;

  @Input()
  values: DropdownValue[];

  @Input()
  placeholder: string;

  @Output()
  select: EventEmitter<any>;

  constructor() {
    this.select = new EventEmitter();
  }

  // selectItem(value) {
  //   alert(value);
  //   this.select.emit(value);
  //   console.log(value);
  // }

  selectItemVal(){
    alert(this.simpleValue);
  }

  selectedItemId: any = null;
  selectedItem($event) {
    this.selectedItemId = $event.target.value;
    console.log($event.target.value);
    console.log(this.isSelected());
  }

  isSelected() {
    console.log(this.selectedItemId);
    return this.selectedItemId == "null" ? false : true;
  }
}