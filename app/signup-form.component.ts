import {Component} from 'angular2/core';
import {ControlGroup, Control, Validators, FormBuilder} from 'angular2/common';
import {UsernameValidator} from './usernameValidator'
import{DropdownValue} from './dropdownvalue';
import {DropdownComponent} from './dropdowncomponent'


@Component({
    selector: 'signup-form',
    templateUrl: 'app/signup-form.component.html',
    directives:[DropdownComponent]
})
export class SignUpFormComponent {
    form: ControlGroup;
    dropdownValues = [new DropdownValue('1', 'XXXXXXX'), new DropdownValue('2', 'YYYYYY')];
txt = "Select Any Vinay";
    constructor(fb: FormBuilder){
        this.form = fb.group({
            username:['', Validators.compose([Validators.required, 
            UsernameValidator.containsSpace]), UsernameValidator.shouldBeUnique],
            password:['', Validators.required]
        });

    }

    // form = new ControlGroup({
    //     username: new Control('', Validators.required),
    //     password: new Control('', Validators.required),
    // });

    submitForm(){
        
        

        this.form.find("username").setErrors({
            invalidLogin: true
        });
        alert("Thanks for signing up");
    }
}