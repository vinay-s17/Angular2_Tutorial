System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var DropdownValue;
    return {
        setters:[],
        execute: function() {
            class DropdownValue {
                constructor(value, label) {
                    this.value = value;
                    this.label = label;
                }
            }
            exports_1("DropdownValue", DropdownValue);
        }
    }
});
//# sourceMappingURL=dropdownvalue.js.map