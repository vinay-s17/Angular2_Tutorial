import {Directive, ElementRef, Renderer} from 'angular2/core'
@Directive({
    selector: '[autoGrow]',
    host:{
            '(focus)': 'onFocus()',
            '(blur)': 'onBlur()'
        }
})
export class AutoGrowDirective{
    constructor(private elElementRef:ElementRef,private  renderer: Renderer){
    }
    onFocus(){
        this.renderer.setElementStyle(this.elElementRef.nativeElement, 'width', '200');
    }
    onBlur(){
        this.renderer.setElementStyle(this.elElementRef.nativeElement, 'width', '120');
    }

}