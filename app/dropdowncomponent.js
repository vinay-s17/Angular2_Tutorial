System.register(['angular2/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var DropdownComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            let DropdownComponent = class DropdownComponent {
                constructor() {
                    this.selectedItemId = null;
                    this.select = new core_1.EventEmitter();
                }
                // selectItem(value) {
                //   alert(value);
                //   this.select.emit(value);
                //   console.log(value);
                // }
                selectItemVal() {
                    alert(this.simpleValue);
                }
                selectedItem($event) {
                    this.selectedItemId = $event.target.value;
                    console.log($event.target.value);
                    console.log(this.isSelected());
                }
                isSelected() {
                    console.log(this.selectedItemId);
                    return this.selectedItemId == "null" ? false : true;
                }
            };
            __decorate([
                core_1.Input(), 
                __metadata('design:type', Array)
            ], DropdownComponent.prototype, "values", void 0);
            __decorate([
                core_1.Input(), 
                __metadata('design:type', String)
            ], DropdownComponent.prototype, "placeholder", void 0);
            __decorate([
                core_1.Output(), 
                __metadata('design:type', core_1.EventEmitter)
            ], DropdownComponent.prototype, "select", void 0);
            DropdownComponent = __decorate([
                core_1.Component({
                    selector: 'st-dropdown',
                    template: `
  <select [(ngModel)]="selectedItemId" (change)="selectedItem($event)" 
  class="selectpicker" data-style="btn-new">
  <option value=null selected="selected">{{placeholder}}</option>
  <option *ngFor="#obj of values" [value]="obj.value">{{obj.label}}</option>
</select>

  `
                }), 
                __metadata('design:paramtypes', [])
            ], DropdownComponent);
            exports_1("DropdownComponent", DropdownComponent);
        }
    }
});
//# sourceMappingURL=dropdowncomponent.js.map