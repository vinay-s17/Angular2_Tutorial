import {Component} from 'angular2/core';
import {CoursesComponent} from './courses.component'
import {AuthorComponent} from './author.component'
import {FavoriteComponent} from './favorite.component'
import {TwitterLikeComponent} from './twitterlike.component'
import {ContactFormComponent} from './contact-form.component'
import {SignUpFormComponent} from './signup-form.component'
import {DropdownComponent} from './dropdowncomponent'
import{DropdownValue} from './dropdownvalue';
import{CounterInputComponent} from './counterinputcomponent';

@Component({
    selector: 'my-app',
    template: `<!--
                    <h1>My First Angular 2 App Vinay Edition
                    <courses></courses><authors></authors>
                    </h1>
               
                    <h1> 
                    <i class="glyphicon glyphicon-star"></i>
                    <favo [is-empty]="post.isEmptyStarr" 
                    (change)="clickHandler($event)"></favo> 
                    <twitterHeart></twitterHeart>
                    <p>{{noOfLikes}}</p>
                    </h1>
                    <contact-form></contact-form>
                    <signup-form></signup-form>
                -->
                <form #form="ngForm" (ngSubmit)="submit(form.value)">
                <counter-input name="counter" ngModel></counter-input>
                <button type="submit">Submit</button>
                </form>
                `,
    directives:[SignUpFormComponent, DropdownComponent, CounterInputComponent]
        //ContactFormComponent,
        //CoursesComponent, AuthorComponent, FavoriteComponent, TwitterLikeComponent]
})
export class AppComponent { 

//    dropdownValues = [new DropdownValue('1', 'XXXXXXX'), new DropdownValue('2', 'YYYYYY')];

    post = {
        isEmptyStarr: false,
        title: "welcome"
    }
    twitterPost = {
        heartDisLike: true,
        noOfLikes :10

    }
    clickHandler($event)
    {
        console.log($event);
    }
}