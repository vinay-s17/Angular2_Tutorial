System.register(['angular2/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var TwitterLikeComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            let TwitterLikeComponent = class TwitterLikeComponent {
                constructor() {
                    this.noOfLikes = 10;
                    this.heartDisLike = true;
                }
                onHeartClick() {
                    this.heartDisLike = !this.heartDisLike;
                    this.noOfLikes += this.heartDisLike ? -1 : 1;
                }
            };
            __decorate([
                core_1.Input(), 
                __metadata('design:type', Object)
            ], TwitterLikeComponent.prototype, "noOfLikes", void 0);
            __decorate([
                core_1.Input(), 
                __metadata('design:type', Object)
            ], TwitterLikeComponent.prototype, "heartDisLike", void 0);
            TwitterLikeComponent = __decorate([
                core_1.Component({
                    selector: 'twitterHeart',
                    template: `
        <i class="glyphicon glyphicon-heart" 
        [class.highighted] = "heartDisLike"
        (click) = "onHeartClick()"> </i>
        <h1>{{noOfLikes}}</h1>
    `,
                    styles: [`
    .glyphicon-heart{
        color: #fff;
        cursor: pointer
    }
    .highighted{
        color: deeplink;
    }
    `]
                }), 
                __metadata('design:paramtypes', [])
            ], TwitterLikeComponent);
            exports_1("TwitterLikeComponent", TwitterLikeComponent);
        }
    }
});
//# sourceMappingURL=twitterlike.component.js.map