import {Component, Input, Output} from 'angular2/core'

@Component({
    selector: 'twitterHeart',
    template:`
        <i class="glyphicon glyphicon-heart" 
        [class.highighted] = "heartDisLike"
        (click) = "onHeartClick()"> </i>
        <h1>{{noOfLikes}}</h1>
    `,
    styles: [`
    .glyphicon-heart{
        color: #fff;
        cursor: pointer
    }
    .highighted{
        color: deeplink;
    }
    `]
})
export class TwitterLikeComponent{
    @Input() noOfLikes = 10;
    @Input() heartDisLike = true;
    onHeartClick()
    {
        this.heartDisLike = !this.heartDisLike;
        this.noOfLikes += this.heartDisLike ? -1 : 1
    }
}