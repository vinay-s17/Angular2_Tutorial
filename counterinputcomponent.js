System.register(['@angular/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var CounterInputComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            let CounterInputComponent = class CounterInputComponent {
                constructor() {
                    this.counterValue = 0;
                }
                increment() {
                    this.counterValue++;
                }
                decrement() {
                    this.counterValue--;
                }
            };
            __decorate([
                core_1.Input(), 
                __metadata('design:type', Object)
            ], CounterInputComponent.prototype, "counterValue", void 0);
            CounterInputComponent = __decorate([
                core_1.Component({
                    selector: 'counter-input',
                    template: `
    <button (click)="increment()">+</button>
    {{counterValue}}
    <button (click)="decrement()">-</button>
  `
                }), 
                __metadata('design:paramtypes', [])
            ], CounterInputComponent);
        }
    }
});
//# sourceMappingURL=counterinputcomponent.js.map